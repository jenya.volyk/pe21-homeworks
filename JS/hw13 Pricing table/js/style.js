'use strict';
window.onload = function () {
    const themeBtn = document.getElementById('theme-button');
    const cssFile = document.getElementById('css');

    function getTheme(num) {
        localStorage.setItem('themeOption', num);
        cssFile.setAttribute('href', `./css/style${num}.css`);
    }

    themeBtn.addEventListener('click', () => {
        let themeOption = localStorage.getItem('themeOption');
        if (themeOption === '1') {
            getTheme(2)
        } else {
            getTheme(1)
        }
    });
    let defaultTheme = localStorage.getItem('themeOption');

        if (defaultTheme === '1') {
            getTheme(1)
        } else {
            getTheme(2)
        }
};