'use strict';

function StringsToListElements(stringArray) {
    return stringArray.map(str => {
            const listText = document.createTextNode(str);
            const listElement = document.createElement('li');
            listElement.appendChild(listText);
            return listElement;
        }
    );
}

const wrapper = document.createElement('div');
const body = document.getElementsByTagName('body')[0];
const list = document.createElement('ul');
const listElements = StringsToListElements(['1', '2', '3', 'sea', 'user', 23]);

listElements.forEach(e => list.appendChild(e));

wrapper.appendChild(list);
body.appendChild(wrapper);

