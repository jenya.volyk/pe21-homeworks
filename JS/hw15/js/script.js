'use strict';

let userInput = prompt('Please enter your number', '');

while (isNaN(userInput) || !userInput) {
    userInput = prompt('Error. Please enter your number', userInput)
}

function calcFactorial(f) {
    if (f === 1) {
        return f;
    } else {
        return f * calcFactorial(f - 1);
    }
}

alert(calcFactorial(userInput));