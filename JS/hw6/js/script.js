'use strict';

function filterBy(arr, dataType) {
    if (dataType === 'null') {
        return arr.filter((value) => (value !== null));
    }
    return arr.filter((value) => (typeof value !== dataType));
}

const initialArray = ['pensil', 174, true, 0, 'hobbit', null, '5', 45, undefined, 150, {
    username: 'Alex',
    age: 15
}, false, NaN, {age: 25}];
let dataType = 'null';
console.log(filterBy(initialArray, dataType));
