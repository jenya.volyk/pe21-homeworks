'use strict';

let userInput = +prompt('Please enter your number', '');

if (userInput >= 5) {
    for (let i = 0; i <= userInput; i += 5) {
        console.log(i);
    }
} else {
    console.log('Sorry, no numbers')
}
// additional task 1

while (!Number.isInteger(userInput)) {
    userInput = +prompt('Please enter integer number', '8');
}

// additional task 2

let m = +prompt('Please enter first prime number', '');
let n = +prompt('Please enter second prime number bigger than previous', '');
let x = 0;
let y = 0;

if (m === 2) {
    x = 1;
} else {
    for (let i = 2; i < m; i++) {
        if (m % i === 0) {
            x = 0;
            break;
        }
        x = 1;
    }
}

for (let j = 2; j < n; j++) {
    if (n % j === 0) {
        y = 0;
        break;
    }
    y = 1;
}

while (m > n || x === 0 || y === 0 || m === 1 || n === 1) {
    if (m === 1 || n === 1) {
        alert('Error! "1" is not a prime number!');
    } else if (m > n) {
        alert('Error! First number is bigger than second!');
    } else {
        alert('Error! First and second number is not prime or both of them!');
    }
    m = +prompt('Please enter first PRIME number', '');
    n = +prompt('Please enter second PRIME number BIGGER than previous', '');

    if (m === 2) {
        x = 1;
    } else {
        for (let i = 2; i < m; i++) {
            if (m % i === 0) {
                x = 0;
                break;
            }
            x = 1;
        }
    }

    for (let j = 2; j < n; j++) {
        if (n % j === 0) {
            y = 0;
            break;
        }
        y = 1;
    }

}
outer:
    for (m; m <= n; m++) {
        for (let i = 2; i < m; i++) {
            if (m % i === 0) continue outer;
        }
        console.log(m)
    }
