'use strict';
window.onload = function () {

    const startButton = document.getElementById('start');
    const stopButton = document.getElementById('stop');

    function showButtons() {
        startButton.classList.add('shown');
        stopButton.classList.add('shown');
    }

    setTimeout(showButtons, 1000);


    let displayedElement = document.getElementsByClassName("shown")[0];

    function reset(prevElem) {
        if (prevElem) {
            prevElem.classList.remove('shown')
        }
        let nextElem = displayedElement.nextElementSibling;
        if (nextElem === null) {
            nextElem = document.getElementsByClassName('image-to-show')[0];
        }
        nextElem.classList.add('shown');
        displayedElement = nextElem;
    }

    let renderIntervalTimer = setInterval(() => reset(displayedElement), 1000);

    stopButton.addEventListener('click', () => clearInterval(renderIntervalTimer));

    startButton.addEventListener('click', () => renderIntervalTimer = setInterval(() => reset(displayedElement), 1000))

};
