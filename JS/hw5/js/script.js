'use strict';

function createNewUser() {
    let userName = prompt('Please enter your name', '');
    let userSurname = prompt('Please enter your surname', '');
    let birthday = prompt('Please enter your birthday in dd.mm.yyyy format');
    const newUser = {
        firstName: userName,
        lastName: userSurname,
        birthday,
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function () {
            let now = new Date();
            let currentYear = now.getFullYear();
            let inputDate = +this.birthday.substring(0, 2);
            let inputMonth = +this.birthday.substring(3, 5);
            let inputYear = +this.birthday.substring(6, 10);
            let birthDate = new Date(inputYear, inputMonth - 1, inputDate);
            let birthYear = birthDate.getFullYear();
            let age = currentYear - birthYear;
            if (now < new Date(birthDate.setFullYear(currentYear))) {
                age = age - 1;
            }
            return age;
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6, 10)
        }

    };
    console.log(newUser.getLogin());
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
    return newUser;
}

createNewUser();