'use strict';
window.onload = function () {
    const visibilitySwitcher = document.getElementsByClassName('fas')[0];
    const confirmationPasswordSwitcher = document.getElementsByTagName('i')[1];
    const firstPasswordInput = document.getElementsByTagName('input')[0];
    const confirmationPasswordInput = document.getElementsByTagName('input')[1];

    function typeChanging(passwordField, switcher) {
        let inputType = passwordField.getAttribute('type');
        if (inputType === 'password') {
            passwordField.setAttribute('type', 'text');
            switcher.classList.remove('fa-eye');
            switcher.classList.add('fa-eye-slash');
        } else {
            passwordField.setAttribute('type', 'password');
            switcher.classList.remove('fa-eye-slash');
            switcher.classList.add('fa-eye');
        }
    }

    visibilitySwitcher.addEventListener('click', () => typeChanging(firstPasswordInput, visibilitySwitcher));
    confirmationPasswordInput.addEventListener('click', () => typeChanging(confirmationPasswordInput, confirmationPasswordSwitcher));

    const submitButton = document.getElementsByClassName('btn')[0];
    const errorText = document.createElement('p');
    errorText.innerText = 'Please enter the same password in the fields';
    errorText.hidden = true;
    errorText.style.color = 'red';
    document.body.append(errorText);

    submitButton.addEventListener('click', (event) => {
        event.preventDefault();
        let value1 = firstPasswordInput.value;
        let value2 = confirmationPasswordInput.value;
        if (value1 === value2) {
            errorText.hidden = true;
             alert('you are welcome');
        } else {
            errorText.hidden = false;
        }
    });
};

