'use strict';

function createNewUser() {
    let userName = prompt('Please enter your name', '');
    let userSurname = prompt('Please enter your surname', '');

    return {
        firstname: userName,
        lastname: userSurname,
        getLogin: function () {
            return this.firstname[0].toLowerCase() + this.lastname.toLowerCase();
        }
    };
   }

console.log(createNewUser().getLogin());