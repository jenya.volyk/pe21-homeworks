'use strict';

function calcFibonacci(F0, F1, n, F2) {
    for (let i = 1; i <= n; i++) {
        F2 = F1 + F0;
        F0 = F1;
        F1 = F2;
    }
    return (F2);
}

function calcFibonacciMinus(F1, F2, n, F3) {
    for (let i = -1; i >= n; i--) {
        if (F1 >= F2) {
            F3 = F1 - F2;
        } else {
            F3 = F2 - F1;
        }
        F1 = F2;
        F2 = F3;
    }
    return F3;
}

let F0 = +prompt('Please enter first number', '');

while (!Number.isInteger(F0) || !F0 ) {
    F0 = +prompt('Please enter your first INTEGER NUMBER')
}
let F1 = +prompt('Please enter second number', '');
while (!Number.isInteger(F1) || !F1 ) {
    F1 = +prompt('Please enter your second INTEGER NUMBER')
}
let n = +prompt('Please enter Fibonacci sequence number ', '');
while (!Number.isInteger(n) || !n || n == 0) {
    n = +prompt('Please enter Fibonacci sequence number, don`t loose your chance')
}
if (n < 0) {
    alert(calcFibonacciMinus(F0, F1, n));
} else {
    alert(calcFibonacci(F0, F1, n));
}