$(document).ready(function () {
    $('#navbar').on('click', 'a', event => {
        event.preventDefault();
        let anchor = $(event.target).attr('href'),
            top = $(anchor).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});

$(function () {
    $.fn.scrollToTop = function () {
        if ($(window).scrollTop() >= '400') $(this).fadeIn('slow')
        let scrollDiv = $(this);
        $(window).scroll(function () {
            if ($(window).scrollTop() <= '400') $(scrollDiv).fadeOut('slow')
            else $(scrollDiv).fadeIn('slow')
        });
        $(this).click(function () {
            $('html, body').animate({scrollTop: 0}, 'slow')
        })
    }
});

$(() => $("#backToTop").scrollToTop());

$("#hiddenBlock").click(() => $("#posts").slideToggle('slow'));