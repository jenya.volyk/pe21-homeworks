'use strict';
window.onload = function () {

    const buttonsCollection = document.getElementsByClassName('btn');
    const buttonsArray = Array.from(buttonsCollection);

    let buttons = {};
    buttons = buttonsArray.reduce((prev, current) => {
        prev[current.dataset.eventname] = current;
        return prev;
    },{});

    let classOwner;

    document.addEventListener('keyup', (event) => {
        if (classOwner) {
            classOwner.classList.remove('highlight');
        }
        if (event.code in buttons) {
            classOwner = buttons[event.code];
            classOwner.classList.add('highlight');
        }
    });
};