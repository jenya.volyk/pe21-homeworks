'use strict';

let num1 = prompt('Please enter first number', '');
let num2 = prompt('Please enter second number', '');

while (isNaN(num1) || isNaN(num2) || !num1 || !num2) {
    alert('Error! Please enter numbers');
    num1 = prompt('Please enter first number', num1);
    num2 = prompt('Please enter second number', num2);
}

let mathOperator = prompt('Please enter math operator', '');

while (mathOperator !== '+' && mathOperator !== '-' && mathOperator !== '*' && mathOperator !== '/') {
    mathOperator = prompt('Error! Incorrect operator. Please enter "+" or "-" or "*" or "/"', mathOperator);
}

function calcNumbers(num1, num2, mathOperator) {
    let result;
    switch (mathOperator) {
        case "+":
            result = num1 + num2;
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            result = num1 / num2;
            break;
        default:
            alert('Hmmm, something went wrong. Please check you internet connection and try later'); /* никогда не появится, проверка выполнялась ранее*/
    }
    return result;
}

console.log(calcNumbers(num1, num2, mathOperator));