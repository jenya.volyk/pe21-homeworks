'use strict';

    const tabList = document.getElementsByClassName('tabs')[0];
    const tabContentList = document.getElementsByClassName('tabs-content')[0];
    let lastActiveTab;

    for (let i = 0; i < tabList.childElementCount && i < tabContentList.childElementCount; ++i) {
        let tab = tabList.children[i];
        let tabTextElement = tabContentList.children[i];

        if (tab.classList.contains('active')) {
            lastActiveTab = tab;
        } else {
            tabTextElement.classList.add('nodisplay');
        }

        tabTextElement.setAttribute('data-tab-number', i);
        tab.setAttribute('data-tab-number', i);

        tab.addEventListener('click', onTabSelect);
    }

    for (let tab of tabList.children) {
        if (tab.classList.contains('active')) {
            lastActiveTab = tab;
        }
        tab.addEventListener('click', onTabSelect);
    }

    function onTabSelect(event) {
        let targetTab = event.target;

        let previousTextBlock = tabContentList.children[lastActiveTab.dataset.tabNumber];
        let nextTextBlock = tabContentList.children[targetTab.dataset.tabNumber];

        previousTextBlock.classList.add('nodisplay');
        nextTextBlock.classList.remove('nodisplay');

        lastActiveTab.classList.remove('active');
        targetTab.classList.add('active');

        lastActiveTab = targetTab;
    }