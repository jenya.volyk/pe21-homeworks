'use strict';

let student = {
    'name': '',
    'last name': '',
};

function getStudentData() {
    student['name'] = prompt('Please, enter your name');
    student['last name'] = prompt('Please, enter your last name');
}

getStudentData();


function getSubjectMarks() {
    let tabel = {};
    let discipline;
    let mark;
    for (; ;) {
        discipline = prompt('Enter discipline');
        if (discipline === null) {
            break
        }
        mark = +prompt(`Enter ${discipline} mark`);
        tabel[discipline] = mark;
    }

    return tabel;
}

student['tabel'] = getSubjectMarks();


let checkedMarks = [];

function checkMarks() {
    let promotionPossibility;
    for (let key in student['tabel']) {
        if (student['tabel'][key] >= 4) {
            checkedMarks.push(student['tabel'][key]);
        } else {
            promotionPossibility = 'rejected'
        }
    }

    if (promotionPossibility === 'rejected') {
        alert(`Студент ${student["name"]} ${student["last name"]} остался на второй год`);
    } else {
        alert(`Студент ${student["name"]} ${student["last name"]} переведен на следующий курс`);

        let sum = checkedMarks.reduce((accumulator, currentValue) => {
            return currentValue + accumulator;
        });

        let averageNum = sum / checkedMarks.length;
        if (averageNum > 7) {
            alert(`Студент ${student['name']} ${student['last name']} будет получать стипендию, средний бал - ${averageNum.toFixed(1)}`)
        }
    }
}

checkMarks();
