'use strict';

const userInputPrice = document.getElementById('price');
const spanCreator = document.createElement('span');
const deleteButton = document.createElement('button');


userInputPrice.addEventListener('focus', ev => {
    userInputPrice.classList.add('border-green')
});
userInputPrice.addEventListener('blur', ev => {
    userInputPrice.classList.remove('border-green');
    if (Number(userInputPrice.value) >= 0) {
        document.body.prepend(deleteButton);
        document.body.prepend(spanCreator);
        spanCreator.innerText = `Current price: ${ev.currentTarget.value} `;
        deleteButton.textContent = 'X';
        deleteButton.classList.add('button-form');
        userInputPrice.classList.add('background-input');
    } else {
        userInputPrice.classList.add('border-red');
        document.body.append(spanCreator);
        spanCreator.innerText = 'Please enter correct price';
    }
});
deleteButton.addEventListener('click', ev => {
    deleteButton.remove();
    spanCreator.remove();
    userInputPrice.value = '';
    userInputPrice.classList.remove('background-input');
});

