const gulp = require('gulp'),
    concat = require('gulp-concat'),
    minify = require('gulp-js-minify'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    cleanCSS = require('gulp-clean-css');
// autoprefixer = require('gulp-autoprefixer');

const paths = {
    src: {
        styles: 'src/scss/**/*.scss',
        script: 'src/js/**/*.js',
        img: 'src/img/**/*'
    },
    dist: {
        styles: "dist/css",
        script: "dist/js",
        img: "dist/img"
    }
};


const buildJS = () => (
    gulp.src(paths.src.script)
        .pipe(concat('script.min.js'))
        .pipe(minify())
        .pipe(gulp.dest(paths.dist.script))
);

const buildSCSS = () => (
    gulp.src(paths.src.styles)
        .pipe(sass().on('error', sass.logError))
        // .pipe(autoprefixer({
        //     cascade: false
        // }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(paths.dist.styles))

);
const runDev = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch(paths.src.styles, buildSCSS).on('change', browserSync.reload);
    gulp.watch(paths.src.script, buildJS).on('change', browserSync.reload)

};

gulp.task('buildJS', buildJS);
gulp.task('buildSCSS', buildSCSS);

gulp.task('build', gulp.series(
    buildJS,
    buildSCSS,
    // buildImg
));
gulp.task('dev', gulp.series(
    buildSCSS,
    buildJS,
    //buildImg
    runDev
));