'use strict';

function applyHiddenClass(arrayForApply, length = arrayForApply.length, count = 0) {
    for (let i = count; i < length; i++) {
        arrayForApply[i].style.display = 'none';
    }
}

const tabsMenu = [...(document.getElementsByClassName('tabs__menu-element'))];

function tabsSwitcher() {
    const dataNumber = this.getAttribute('data-number');
    const tabsContent = [...(document.getElementsByClassName('tabs__content'))];

    if (this.classList.contains('active-bright')) {
        return;
    }

    tabsMenu.forEach((value) => value.classList.remove('active-bright'));
    this.classList.add('active-bright');

    tabsContent.forEach((value) => value.style.display = 'none');

    const dataNumberServices = tabsContent.filter((items) => {
        return items.getAttribute('data-number') === dataNumber;
    });

    dataNumberServices[0].style.display = '';
}

const buttonLoadMore = document.getElementsByClassName('load-more')[0];
const itemsImage = [...(document.getElementsByClassName('filter-tabs__image-container'))];
const menuElements = [...(document.getElementsByClassName('filter-tabs__menu-list-element'))];


function showCategoryImages() {
    const tabsImageWrapper = [...(document.getElementsByClassName('filter-tabs__image-wrapper'))];
    buttonLoadMore.style.display = '';
    const showItems = 12;

    tabsImageWrapper.forEach((value) => value.classList.remove('margin-bottom'));
    menuElements.forEach((value) => value.classList.remove('active-light'));

    this.classList.add('active-light');

    const imageCategory = this.getAttribute('data-image-category');

    itemsImage.forEach((value) => value.style.display = 'none');

    const dataItemsImage = itemsImage.filter((items) => {
        return items.getAttribute('data-image-category') === imageCategory;
    });

    const hideButtonAddClass = () => {
        buttonLoadMore.style.display = 'none';
        tabsImageWrapper[0].classList.add('margin-bottom');
    };
    if (dataItemsImage.length > showItems) {
        for (let i = 0; i < showItems; i++) {
            dataItemsImage[i].style.display = '';
        }
        buttonLoadMore.style.display = '';
    } else if (imageCategory === 'all') {
        itemsImage.forEach((value) => value.style.display = '');
        hideButtonAddClass();
    } else {
        dataItemsImage.forEach((value) => value.style.display = '');
        hideButtonAddClass();
    }
}

const loader = document.getElementsByClassName('loader')[0];

function showMoreImages() {
    const tabsImageWrapper = document.getElementsByClassName('filter-tabs__image-wrapper')[0];
    const hiddenElem = itemsImage.filter((value) => {
        return value.style.display === 'none';
    });

    const showItems = 12;
    const dataImageItem = document.getElementsByClassName('active-light')[0].getAttribute('data-image-category');
    const allElemDataItems = itemsImage.filter((items) => {
        return items.getAttribute('data-image-category') === dataImageItem;
    });

    buttonLoadMore.style.display = 'none';
    loader.style.display = '';

    setTimeout(function () {
        buttonLoadMore.style.display = '';
        loader.style.display = 'none';
        if (hiddenElem.length === showItems) {
            buttonLoadMore.style.display = 'none';
            tabsImageWrapper.classList.add('margin-bottom');
        }
        if (dataImageItem === 'all') {
            for (let i = 0; i < showItems; i++) {
                hiddenElem[i].style.display = '';
            }
        } else {
            for (let i = showItems; i <= allElemDataItems.length - 1; i++) {
                allElemDataItems[i].style.display = '';
            }
            buttonLoadMore.style.display = 'none';
            tabsImageWrapper.classList.add('margin-bottom');
        }
    }, 1000);
}

function showRespondents() {
    const miniatureRespondents = [...(document.getElementsByClassName('miniature'))];
    const fullRespondentsInfo = [...(document.getElementsByClassName('reviews__respondent-container'))];

    fullRespondentsInfo.forEach((value) => value.style.display = 'none');

    const dataRespondents = this.getAttribute('data-respondent');
    const chosenRespondentReview = fullRespondentsInfo.filter((value) => {
        return value.getAttribute('data-respondent') === dataRespondents;
    });

    chosenRespondentReview[0].style.display = '';
    miniatureRespondents.forEach((value) => value.classList.remove('miniature-border'));
    this.classList.add('miniature-border');
}


function moveLeft() {
    const miniImages = [...(document.getElementsByClassName('miniature'))];
    const currentImageIndex = miniImages.findIndex((items) => items.classList.contains('miniature-border'));
    const currentImage = document.getElementsByClassName('miniature-border')[0];
    let prevImageIndex = currentImageIndex - 1;
    let prevImage = miniImages[prevImageIndex];

    const fullRespondentsInfo = [...(document.getElementsByClassName('reviews__respondent-container'))];
    fullRespondentsInfo.forEach((items) => items.style.display = 'none');

    if (currentImageIndex === 0) {
        miniImages.forEach((items) => items.style.display = '');
        prevImage = miniImages[miniImages.length - 1];
        prevImageIndex = miniImages.length - 1;
        currentImage[currentImageIndex].style.display = 'none';
    }
    if (prevImage.style.display === 'none') {
        prevImage.style.display = '';
        miniImages[currentImageIndex + 3].style.display = 'none';
    }
    currentImage.classList.remove('miniature-border');
    prevImage.classList.add('miniature-border');
    fullRespondentsInfo[prevImageIndex].style.display = '';
}

function moveRight() {
    const miniImages = [...(document.getElementsByClassName('miniature'))];
    const currentImageIndex = miniImages.findIndex((items) => items.classList.contains('miniature-border'));
    const currentImage = document.getElementsByClassName('miniature-border')[0];
    let nextImageIndex = currentImageIndex + 1;
    let nextImage = miniImages[nextImageIndex];

    const fullRespondentsInfo = [...(document.getElementsByClassName('reviews__respondent-container'))];
    fullRespondentsInfo.forEach((items) => items.style.display = 'none');

    if (currentImageIndex === (miniImages.length - 1)) {
        miniImages.forEach((items) => items.style.display = '');
        nextImage = miniImages[0];
        nextImageIndex = 0;
        currentImage.style.display = 'none';
    }
    if (nextImage.style.display === 'none') {
        nextImage.style.display = '';
        miniImages[currentImageIndex - 3].style.display = 'none';
    }
    currentImage.classList.remove('miniature-border');
    nextImage.classList.add('miniature-border');
    fullRespondentsInfo[nextImageIndex].style.display = '';
}

const htmlLink = [...(document.getElementsByTagName('a'))];
const hideRespondents = document.getElementsByClassName('hide-respondent');
const hideRespondentMiniature = document.getElementsByClassName('hide-miniature');
const hidden = document.getElementsByClassName('hidden');
const hideBestImage = document.getElementsByClassName('hide-best-image');
const miniatures = [...(document.getElementsByClassName('miniature'))];
const loadMoreImages = document.getElementsByClassName('load-more')[0];
const leftButton = document.getElementsByClassName('fa-angle-left');
const rightButton = document.getElementsByClassName('fa-angle-right');

window.onload = function() {
    htmlLink.forEach((value) => value.addEventListener('click', (e) => e.preventDefault()));
    applyHiddenClass(hideRespondents);
    applyHiddenClass(hideRespondentMiniature);
    applyHiddenClass(itemsImage, itemsImage.length, 12);
    applyHiddenClass(hideBestImage);
    applyHiddenClass(hidden);
    tabsMenu.forEach((value) => value.addEventListener('click', tabsSwitcher));
    menuElements.forEach((value) => value.addEventListener('click', showCategoryImages));
    loadMoreImages.addEventListener('click', showMoreImages);
    miniatures.forEach((value) => value.addEventListener('click', showRespondents));
    leftButton[0].addEventListener('click', moveLeft);
    rightButton[0].addEventListener('click', moveRight);
};